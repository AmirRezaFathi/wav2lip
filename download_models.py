import requests
from tqdm import tqdm


def download_file(url, filename):
    # Send a HTTP request to the server.
    response = requests.get(url, stream=True)

    # Total size in bytes.
    total_size = int(response.headers.get('content-length', 0))
    block_size = 1024  # 1 Kibibyte

    progress_bar = tqdm(total=total_size, unit='iB', unit_scale=True)
    with open(filename, 'wb') as file:
        for data in response.iter_content(block_size):
            progress_bar.update(len(data))
            file.write(data)
    progress_bar.close()

    if total_size != 0 and progress_bar.n != total_size:
        print("ERROR, something went wrong")


# Example usage
if __name__ == "__main__":
    url = ("https://drive.usercontent.google.com/download?"
           "id=1_OvqStxNxLc7bXzlaVG5sz695p-FVfYY&export=download&authuser=0&"
           "confirm=t&uuid=8587aaa0-f951-4386-bc77-7ef10ca0c04e&"
           "at=APZUnTV6N_FCNaF2LAlc90R4SgHH:1708885683363")
    filename = "sd-wav2lip-uhq/scripts/wav2lip/checkpoints/wav2lip_gan.pth"
    download_file(url, filename)

    url = ("https://drive.usercontent.google.com/download?"
           "id=1_OvqStxNxLc7bXzlaVG5sz695p-FVfYY&export=download&authuser=0&"
           "confirm=t&uuid=8587aaa0-f951-4386-bc77-7ef10ca0c04e&"
           "at=APZUnTV6N_FCNaF2LAlc90R4SgHH:1708885683363")
    filename = "wav2lip_gan.pth"
    download_file(url, filename)

    url = ("https://iiitaphyd-my.sharepoint.com/personal/radrabha_m_research_iiit_ac_in/"
           "_layouts/15/download.aspx?SourceUrl=%2Fpersonal%2Fradrabha%5Fm%5Fresearch%5Fiiit"
           "%5Fac%5Fin%2FDocuments%2FWav2Lip%5FModels%2Fvisual%5Fquality%5Fdisc%2Epth")
    filename = "visual_quality_disc.pth"
    download_file(url, filename)

    url = ("https://iiitaphyd-my.sharepoint.com/personal/"
           "radrabha_m_research_iiit_ac_in/_layouts/15/download.aspx?"
           "SourceUrl=%2Fpersonal%2Fradrabha%5Fm%5Fresearch%5Fiiit%5Fac%5"
           "Fin%2FDocuments%2FWav2Lip%5FModels%2Flipsync%5Fexpert%2Epth")
    filename = "lipsync_expert.pth"
    download_file(url, filename)

    url = ("https://www.adrianbulat.com/downloads/python-fan/s3fd-619a316812.pth")
    filename = "face_detection/detection/sfd/s3fd1.pth"
    download_file(url, filename)

    url = ("https://www.adrianbulat.com/downloads/python-fan/s3fd-619a316812.pth")
    filename = "sd-wav2lip-uhq/scripts/wav2lip/face_detection/detection/sfd/s3fd.pth"
    download_file(url, filename)

    url = ("https://huggingface.co/ezioruan/inswapper_128.onnx/resolve/main/inswapper_128.onnx")
    filename = "sd-wav2lip-uhq/scripts/faceswap/model/inswapper_128.onnx"
    download_file(url, filename)

    url = ("https://raw.githubusercontent.com/italojs/facial-landmarks-recognition/master/shape_predictor_68_face_landmarks.dat")
    filename = "sd-wav2lip-uhq/scripts/wav2lip/predicator/shape_predictor_68_face_landmarks.dat"
    download_file(url, filename)
